﻿using System.Globalization;
using Brain2CPU.ExifTool;

internal static class Program
{
    public static void Main(string[] args)
    {
        string dateTimePattern = "yyyy:MM:dd HH:mm:ss";
        string path = @"D:\Media\Photos\Snapchat - Copy\Movies";
        string[] filePaths = Directory.GetFiles(path);
        var exif = new ExifToolWrapper();
        exif.Start();
        // string typeConstraint = "IMG";
        // string tagConstraint = "Date/Time Original";
        // string tagToWriteConstraint = "datetimeoriginal";
        // string tagToWriteConstraint = "PNG:CreationTime";
        string typeConstraint = "VID";
        string tagConstraint = "Media Create Date";
        string tagToWriteConstraint = "CreateDate";
        foreach (string filePath in filePaths)
        {
            //date taken
            try
            {
                DateTime? dateTaken = null;
                Dictionary<string, string> d = exif.FetchExifFrom(filePath);
                // foreach (KeyValuePair<string, string> kvp in d)
                // {
                // textBox3.Text += ("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                // Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
                // }
                // Console.Read();
                // continue;
                if (d.ContainsKey(tagConstraint) && !String.IsNullOrEmpty(d[tagConstraint]) &&
                    d[tagConstraint] != "0000:00:00 00:00:00")
                {
                    //Console.WriteLine(d[tagConstraint]);
                    dateTaken = DateTime.ParseExact(d[tagConstraint], "yyyy.MM.dd HH:mm:ss",
                        CultureInfo.InvariantCulture);
                    if (typeConstraint == "VID")
                    {
                        dateTaken = ((DateTime)dateTaken).ToLocalTime();
                    }
                }

                /*var file = ImageFile.FromFile(filePath);
                DateTime? dateTaken = null;
                if (file.Properties.Get(ExifTag.DateTimeOriginal) != null)
                {
                    dateTaken = file.Properties.Get<ExifDateTime>(ExifTag.DateTimeOriginal);
                }*/

                //system date
                DateTime systemDate;
                DateTime modificationDate = File.GetLastWriteTime(filePath);
                DateTime createdDate = File.GetCreationTime(filePath);
                if (DateTime.Compare(modificationDate, createdDate) <= 0)
                {
                    systemDate = modificationDate;
                }
                else
                {
                    systemDate = createdDate;
                }

                if (dateTaken != null && DateTime.Compare((DateTime)dateTaken, systemDate) <= 0)
                {
                    systemDate = (DateTime)dateTaken;
                }

                //date in name
                DateTime? nameDate = null;
                string fileName = Path.GetFileName(filePath);
                string unparsedDate = String.Empty;
                string pattern = String.Empty;
                bool nameWithTime = true;
                bool nameHasDate = true;
                if (fileName.StartsWith(typeConstraint + "_"))
                {
                    unparsedDate = fileName.Substring(4, 15);
                    pattern = "yyyyMMdd_HHmmss";
                }
                else if (fileName.StartsWith(typeConstraint + "-") && fileName.IndexOf("-WA") > -1)
                {
                    unparsedDate = fileName.Substring(4, 8);
                    pattern = "yyyyMMdd";
                    nameWithTime = false;
                }
                else if (fileName.StartsWith("20"))
                {
                    unparsedDate = fileName.Substring(0, 19);
                    pattern = "yyyy-MM-dd_HH-mm-ss";
                }
                else if (fileName.IndexOf("_20") > -1)
                {
                    unparsedDate = fileName.Substring(fileName.IndexOf("_20") + 1, 19);
                    pattern = "yyyy-MM-dd_HH-mm-ss";
                }
                else
                {
                    nameHasDate = false;
                }

                if (nameHasDate)
                {
                    nameDate = DateTime.ParseExact(unparsedDate, pattern, CultureInfo.InvariantCulture);
                }

                // bool sameSystem = false;
                // bool sameName = false;

                // if (dateTaken != null && DateTime.Compare((DateTime)dateTaken, systemDate) == 0)
                // {
                //     sameSystem = true;
                // }

                // if (dateTaken != null && nameDate != null && DateTime.Compare((DateTime)nameDate, (DateTime)dateTaken) == 0)
                // {
                //     sameName = true;
                // }

                if (nameDate == null)
                {
                    if (typeConstraint == "VID")
                    {
                        systemDate = systemDate.ToUniversalTime();
                    }

                    var result = exif.SendCommand("-" + tagToWriteConstraint + "=" +
                                                  systemDate.ToString(dateTimePattern) +
                                                  "\n-overwrite_original\n" + filePath);
                    Console.WriteLine(result.IsSuccess);
                    Console.WriteLine(result.Result);
                }
                else if (nameWithTime)
                {
                    if (DateTime.Compare(systemDate, (DateTime)nameDate) <= 0)
                    {
                        if (typeConstraint == "VID")
                        {
                            systemDate = systemDate.ToUniversalTime();
                        }

                        var result = exif.SendCommand("-" + tagToWriteConstraint + "=" +
                                                      systemDate.ToString(dateTimePattern) +
                                                      "\n-overwrite_original\n" + filePath);
                        Console.WriteLine(result.IsSuccess);
                        Console.WriteLine(result.Result);
                    }
                    else
                    {
                        if (typeConstraint == "VID")
                        {
                            nameDate = ((DateTime)nameDate).ToUniversalTime();
                        }

                        var result = exif.SendCommand("-" + tagToWriteConstraint + "=" +
                                                      ((DateTime)nameDate).ToString(dateTimePattern) +
                                                      "\n-overwrite_original\n" +
                                                      filePath);
                        Console.WriteLine(result.IsSuccess);
                        Console.WriteLine(result.Result);
                    }
                }
                else
                {
                    if (systemDate.Date <= ((DateTime)nameDate).Date)
                    {
                        if (typeConstraint == "VID")
                        {
                            systemDate = systemDate.ToUniversalTime();
                        }

                        var result = exif.SendCommand("-" + tagToWriteConstraint + "=" +
                                                      systemDate.ToString(dateTimePattern) +
                                                      "\n-overwrite_original\n" + filePath);
                        Console.WriteLine(result.IsSuccess);
                        Console.WriteLine(result.Result);
                    }
                    else
                    {
                        if (typeConstraint == "VID")
                        {
                            nameDate = ((DateTime)nameDate).ToUniversalTime();
                        }

                        var result = exif.SendCommand("-" + tagToWriteConstraint + "=" +
                                                      ((DateTime)nameDate).ToString(dateTimePattern) +
                                                      "\n-overwrite_original\n" +
                                                      filePath);
                        Console.WriteLine(result.IsSuccess);
                        Console.WriteLine(result.Result);
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex + " - " + filePath);
            }
        }

        exif.Stop();
        exif.Dispose();
    }
}